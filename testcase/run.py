import unittest
import case.Test_cal as t1
import case.istrue as t2
import case


def suite1():
    suite = unittest.TestSuite()
    loader = unittest.TestLoader()
    test1 = loader.loadTestsFromModule(t1)
    test2 = loader.loadTestsFromModule(t2)
    suite.addTests(test1)
    suite.addTests(test2)
    return suite


def suite2():
    suite = unittest.TestSuite()
    loader = unittest.TestLoader()
    test1 = loader.loadTestsFromName('case.Test_cal.A.test1')  # case的路径，一定要加引号
    test2 = loader.loadTestsFromName('case.Test_cal.A.test2')
    suite.addTest(test1)
    suite.addTest(test2)
    return suite

# case.Test_cal.Cl.test1
def ddt():
    suite = unittest.TestSuite()
    loader = unittest.TestLoader()
    test = loader.loadTestsFromName('case.Test_cal.Cl.test1')
    suite.addTest(test)
    return suite


# def suite3():
#     suite = unittest.TestSuite()
#     loder = unittest.TestLoader()
#     test1 = loder.loadTestsFromTestCase()


if __name__ == '__main__':
    suite = ddt()
    with open('report.txt', mode='w', encoding='utf8') as f:
        runner = unittest.TextTestRunner(stream=f, verbosity=2)
        runner.run(suite)
# def suite():
#     suite = unittest.TestSuite()
#
#     loader = unittest.TestLoader()
#     # 从py文件中添加所有测试用用例 模块路径
#     tests1 = loader.loadTestsFromModule(t1)
#     tests2 = loader.loadTestsFromModule(t2)
#
#     suite.addTests(tests1)
#     suite.addTests(tests2)
#
#     return suite
#
#
# def suite2():
#     suite = unittest.TestSuite()
#     loader = unittest.TestLoader()
#     # 给定一个测试路径的名称  tmp.tmp1.TestA.testA_01  加载TestA中的 testA_01 一个方法
#     tests1 = loader.loadTestsFromName('tmp.tmp1.TestA.testA_01')
#     suite.addTests(tests1)
#     return suite
#
#
# def suite3():
#     suite = unittest.TestSuite()
#     loader = unittest.TestLoader()
#     # discover 探索模式，给定目录，从目录中搜索所有的符合条件的用例
#     tests = loader.discover('tmp', pattern='tmp*.py')
#
#     suite.addTests(tests)
#     return suite
