import case.cal as t1
import unittest
from ddt import ddt,data,unpack


@ddt
class Cl(unittest.TestCase):
    @data((3, 4, 7), (5, 6, 11), (8, 9, 2))
    @unpack
    def test1(self, n1, n2, n3):
        sum1 = t1.Add_sub().add(n1, n2)
        self.assertEqual(sum1, n3)

    def test2(self):
        sub = t1.Add_sub().sub(3, 5)
        self.assertEqual(sub, -2)



if __name__ == '__main__':
    unittest.main()
